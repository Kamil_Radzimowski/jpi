package Laby.Laby1

import scala.annotation.tailrec
import scala.language.postfixOps

object Zadanie3 {
  def main(args: Array[String]): Unit = {
    val x = scala.io.StdIn.readInt()
    println(isPrime(x))
  }

  def isPrime(x:Integer): Boolean = {
    val y = math.floor(Math.sqrt(x.doubleValue()))
    primeHelper(x, y)
  }

  @tailrec
  private def primeHelper(x: Integer, y: Double): Boolean = {
    if (y >= x){
      return true
    }
    if((x%y == 0) && (y != 1)){
      return false
    }
    primeHelper(x, y+1)
  }
}
